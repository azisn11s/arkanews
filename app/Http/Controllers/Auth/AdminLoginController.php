<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminLoginController extends Controller
{
	public function __construct()
	{
		// $this->middleware('guest:admin');
	}
    public function showLoginForm()
    {
    		return view('auth.admin-login');
    }

    public function login()
    {
    		// Validate form data

    		// Attempt to log user in

    		// If successful

    		// If unsuccessful
    }
}
